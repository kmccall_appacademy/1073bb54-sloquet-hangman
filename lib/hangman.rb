class Hangman
  attr_reader :guesser, :referee, :board

  DICTIONARY = []

  def initialize(players)
    @players = players
  end

  def self.play
    File.foreach("dictionary.txt") {|line| DICTIONARY << line.chomp}
    game = self.choose_player == "human" ? self.human_guess_computer_word : self.computer_guess_human_word
    game.setup
    puts "The word is #{game.guesser.secret_word_length} letters long"
    puts game.board.join('')
    guesses = 0
    until game.over?
      game.take_turn
      puts game.board.join('')
      guesses += 1
    end
    puts ''
    puts "~~Congratulations!~~"
    puts ''
    puts "       ^___^        "
    puts ''
    puts "It took you #{guesses} guesses."
    puts "Don't bother with a wheel of fortune audition" if guesses > 10
  end

  def self.choose_player
    puts "Who will be guessing, computer or human player?"
    puts "please enter computer or human"
    user = gets.chomp
    until user == "human" || user == "computer"
      puts "Enter 'computer' or 'human'"
      user = gets.chomp
    end
    user
  end

  def over?
    !@board.include?("_")
  end

  def self.human_guess_computer_word
    Hangman.new(guesser: HumanPlayer.new("Human"), referee: ComputerPlayer.new(DICTIONARY))
  end

  def self.computer_guess_human_word
    Hangman.new(guesser: ComputerPlayer.new(DICTIONARY), referee: HumanPlayer.new("Human"))
  end

  def guesser
    @players[:guesser]
  end

  def referee
    @players[:referee]
  end

  def setup
    word_length = referee.pick_secret_word
    guesser.register_secret_length(word_length)
    @board = Array.new(word_length, "_")
  end

  def take_turn
    ltr = guesser.guess(@board)
    matching_idxs = referee.check_guess(ltr)
    update_board(ltr, matching_idxs)
    guesser.handle_response(ltr, matching_idxs)
  end

  def update_board(ltr, matching_idxs)
    matching_idxs.each {|idx| @board[idx] = ltr}
  end

end

class HumanPlayer
  attr_reader :secret_word_length

  def initialize(name)
    @name = name
  end

  def register_secret_length(word_length)
    @secret_word_length = word_length
  end

  def guess(board)
    puts "Guess a letter"
    guess = gets.chomp
    until guess.length == 1
      puts "Guess a SINGLE letter"
      guess = gets.chomp
    end
    guess
  end

  def check_guess(ltr)
    puts "The computer guessed #{ltr}"
    puts "Which indexes contain that letter?"
    puts "Please separate multiple indexes by a comma and space."
    puts "Press enter if no matches"
    indexes = gets.chomp.split(", ")
    indexes.map(&:to_i)
  end

  def handle_response(ltr, matching_idxs)
  end

  def pick_secret_word
    puts "How many letters is your word?"
    secret = gets.chomp.to_i
    until Integer && secret <= 15 && secret > 0
      puts "Enter a realistic number"
      secret = gets.chomp.to_i
    end
    secret
  end
end

class ComputerPlayer
  attr_reader :secret_word_length, :secret_word

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    secret_word = @dictionary.sample
    @secret_word = secret_word
    secret_word.length
  end

  def check_guess(letter)
    idx_arr = (0...@secret_word.length).find_all {|idx| @secret_word[idx] == letter}
  end

  def register_secret_length(length)
    @secret_word_length = length
    @dictionary = @dictionary.select {|word| word.length == length}
  end

  def guess(board)
    letters = @dictionary.join('').chars
    board.each {|chr| letters.delete(chr)}
    letters.max_by {|chr| letters.count(chr)}
  end

  def handle_response(guess, indexes)
    #remove dictionary words(computers guess options) without that letter at those indexes
    @dictionary.each do |word|
      if word.count(guess) != indexes.count
        @dictionary.delete(word)
      end
      unless indexes.all? {|idx| word[idx] == guess}
        @dictionary.delete(word)
      end
    end
  end

  def candidate_words
    @dictionary
  end
end

#Hangman.play
